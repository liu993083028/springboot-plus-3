package com.ibeetl.admin.core.dao;

import org.beetl.sql.mapper.annotation.SqlResource;
import org.beetl.sql.mapper.BaseMapper;

import com.ibeetl.admin.core.entity.CoreAudit;

/*
* 
* gen by starter mapper 2017-08-01
*/
@SqlResource("core.coreAudit")
public interface CoreAuditDao extends BaseMapper<CoreAudit> {
	
}