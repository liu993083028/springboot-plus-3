package com.ibeetl.admin.console.dao;

import java.util.List;

import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.mapper.BaseMapper;
import org.beetl.sql.mapper.annotation.Param;
import org.beetl.sql.mapper.annotation.Select;
import org.beetl.sql.mapper.annotation.SqlResource;

import com.ibeetl.admin.core.entity.CoreOrg;
import com.ibeetl.admin.core.entity.CoreUser;

@SqlResource("console.org")
public interface OrgConsoleDao extends BaseMapper<CoreOrg> {

   
 

    void queryByCondtion(PageQuery<CoreOrg> query);
    @Select
//    @SqlStatement(type=SqlStatementType.SELECT)
    void queryUserByCondtion(PageQuery<CoreUser> query);
    void batchDelByIds(@Param(value = "ids") List<Long> ids);
}
