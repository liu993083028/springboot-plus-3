package com.ibeetl.admin.core.dao;

import org.beetl.sql.mapper.annotation.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.mapper.BaseMapper;

import com.ibeetl.admin.core.entity.CoreUserRole;

@SqlResource("core.coresUserRole")
public interface CoreUserRoleDao extends BaseMapper<CoreUserRole> {


}
