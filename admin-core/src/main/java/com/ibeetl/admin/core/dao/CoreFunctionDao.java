package com.ibeetl.admin.core.dao;

import org.beetl.sql.mapper.annotation.Param;
import org.beetl.sql.mapper.annotation.Sql;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.beetl.sql.mapper.BaseMapper;

import com.ibeetl.admin.core.entity.CoreFunction;

@SqlResource("core.coreFunction")
public interface CoreFunctionDao extends BaseMapper<CoreFunction> {
    @Sql("select * from core_function where code = ?")
    CoreFunction getFunctionByCode(@Param(value = "code") String code);
}
