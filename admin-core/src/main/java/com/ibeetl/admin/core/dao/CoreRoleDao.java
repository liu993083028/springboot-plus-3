package com.ibeetl.admin.core.dao;

import org.beetl.sql.mapper.annotation.Param;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.mapper.BaseMapper;

import com.ibeetl.admin.core.entity.CoreRole;

import java.util.List;

@SqlResource("core.coreRole")
public interface CoreRoleDao extends BaseMapper<CoreRole> {



}
