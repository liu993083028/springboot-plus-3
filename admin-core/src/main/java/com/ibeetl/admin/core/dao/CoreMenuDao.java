package com.ibeetl.admin.core.dao;

import org.beetl.sql.mapper.annotation.Param;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.beetl.sql.core.engine.PageQuery;
import org.beetl.sql.mapper.BaseMapper;

import com.ibeetl.admin.core.entity.CoreMenu;

import java.util.List;

@SqlResource("core.coreMenu")
public interface CoreMenuDao extends BaseMapper<CoreMenu> {

    void queryByCondtion(PageQuery query);

    void clearMenuFunction( List<Long> functionIds);


    List<CoreMenu> allMenuWithURL();

}
