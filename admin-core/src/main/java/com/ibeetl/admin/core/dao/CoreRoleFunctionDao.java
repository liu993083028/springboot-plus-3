package com.ibeetl.admin.core.dao;

import org.beetl.sql.mapper.annotation.Param;
import org.beetl.sql.mapper.annotation.SqlResource;
import org.beetl.sql.mapper.BaseMapper;

import com.ibeetl.admin.core.entity.CoreRoleFunction;

import java.util.List;

@SqlResource("core.coreRoleFunction")
public interface CoreRoleFunctionDao extends BaseMapper<CoreRoleFunction> {


    List<CoreRoleFunction> getRoleFunction( Long userId, Long orgId,
                                         String code);

    List<String> getRoleChildrenFunction(Long userId,  Long orgId,
                                          Long parentId);


}
